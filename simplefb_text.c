/* simplefb_text.c
 *
 * provide simple text rendering for images and framebuffers
 *
 * Gunnar Zötl <gz@tset.de>, 2014
 * Licensed under the terms of the MIT license. See file LICENSE for details.
 */

#include <math.h>
#include <stdlib.h>
#include <stdio.h>

#include "simplefb.h"

extern void fb_seterrstr(const char* str, ...);

#define STB_TRUETYPE_IMPLEMENTATION
#include "stb_truetype.h"

/* text is unicode these days...
 */

// Copyright (c) 2008-2010 Bjoern Hoehrmann <bjoern@hoehrmann.de>
// See http://bjoern.hoehrmann.de/utf-8/decoder/dfa/ for details.

#define UTF8_ACCEPT 0
#define UTF8_REJECT 12

static const uint8_t utf8d[] = {
  // The first part of the table maps bytes to character classes that
  // to reduce the size of the transition table and create bitmasks.
   0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
   0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
   0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
   0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
   1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,  9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,
   7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,  7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,
   8,8,2,2,2,2,2,2,2,2,2,2,2,2,2,2,  2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,
  10,3,3,3,3,3,3,3,3,3,3,3,3,4,3,3, 11,6,6,6,5,8,8,8,8,8,8,8,8,8,8,8,

  // The second part is a transition table that maps a combination
  // of a state of the automaton and a character class to a state.
   0,12,24,36,60,96,84,12,12,12,48,72, 12,12,12,12,12,12,12,12,12,12,12,12,
  12, 0,12,12,12,12,12, 0,12, 0,12,12, 12,24,12,12,12,12,12,24,12,24,12,12,
  12,12,12,12,12,12,12,24,12,12,12,12, 12,24,12,12,12,12,12,12,12,24,12,12,
  12,12,12,12,12,12,12,36,12,36,12,12, 12,36,12,12,12,12,12,36,12,36,12,12,
  12,36,12,12,12,12,12,12,12,12,12,12, 
};

static uint32_t inline
decode(uint32_t* state, uint32_t* codep, uint32_t byte) {
	byte &= 0xff;
  uint32_t type = utf8d[byte];

  *codep = (*state != UTF8_ACCEPT) ?
    (byte & 0x3fu) | (*codep << 6) :
    (0xff >> type) & (byte);

  *state = utf8d[256 + *state + type];
  return *state;
}

static uint32_t utf8NextChar(char** s)
{
	uint32_t state = 0;
	uint32_t codep = 0;
	char *p = *s;

	do {
		decode(&state, &codep, *p++);
	} while (*p != 0 && state != UTF8_ACCEPT && state != UTF8_REJECT);
	
	if (state == UTF8_ACCEPT) {
		*s = p;
		return codep;
	}
	// else
	codep = **s & 0xFF;
	*s += 1;
	return codep;
}

/* font handling and text printing */

static char* _extractfontname(const char *n_in)
{
	char *n_out = NULL;
	int len = strlen(n_in);
	int i = len - 1;
	
	while (n_in[i] != '\\' &&  n_in[i] != '/' && i >= 0) --i;
	int ns = i + 1;
	i = len - 1;
	while (n_in[i] != '.' && i >= 0) --i;
	if (i == 0) i = len - 1;
	int nl = i - ns;
	if (nl > 0) {
		n_out = malloc(nl + 1);
		strncpy(n_out, n_in + ns, nl);
		n_out[nl] = 0;
	}

	return n_out;
}

struct _fb_font {
	const char *name;
	unsigned char* buffer;
	int spacewidth;
	stbtt_fontinfo info;
};

fb_font *fb_txt_loadfont(const char *name)
{
	fb_font *font = NULL;
	
	FILE *f = fopen(name, "r");
	if (f) {
		int fail = 0;
		
		font = calloc(1, sizeof(fb_font));
		fseek(f, 0, SEEK_END);
		int fsize = ftell(f);
		rewind(f);
		
		font->buffer = malloc(fsize);
		if (fread(font->buffer, 1, fsize, f) != fsize)
			fail = 1;
		else {
			fclose(f);
			font->name = _extractfontname(name);
			if (!stbtt_InitFont(&font->info, font->buffer, 0))
				fail = 1;
		}
		if (fail) {
			fb_txt_freefont(font);
			font = 0;
		} else {
			int x0 = 0, y0 = 0, x1 = 0, y1 = 0, adv = 0, lsb = 0;
			int g = stbtt_FindGlyphIndex(&font->info, ' ');
			stbtt_GetGlyphHMetrics(&font->info, g, &adv, &lsb);
			stbtt_GetGlyphBitmapBox(&font->info, g, 1.0, 1.0, &x0, &y0, &x1, &y1);
			font->spacewidth = x0 + adv;
		}
	}
	return font;
}

void fb_txt_freefont(fb_font *f)
{
	if (f->name) free((char*) f->name);
	if (f->buffer) free(f->buffer);
	free(f);
}

const char* fb_txt_getfontname(fb_font *f)
{
	return f->name;
}

static int _getcharinfo(fb_font *f, int size, int ch, int *gw, int *gh, int *asc, int *ybase, int *xofs, int *advance)
{
	int x0, y0, x1, y1;
	/* throwaway values */
	int lsb, desc, lg;
	int g = stbtt_FindGlyphIndex(&f->info, ch);
	if (g) {
		float scale = stbtt_ScaleForPixelHeight(&f->info, size);
		stbtt_GetFontVMetrics(&f->info, asc, &desc, &lg);
		*asc = ceil(*asc * scale);
		stbtt_GetGlyphHMetrics(&f->info, g, advance, &lsb);
		stbtt_GetGlyphBitmapBox(&f->info, g, scale, scale, &x0, &y0, &x1, &y1);
		*gw = x1-x0;
		*gh = y1-y0;
		*ybase = y0;
		*xofs = x0;
		*advance = round(*advance * scale);
	}
	return g;
}

fb_image *fb_txt_render_alpha(fb_font *f, int size, const char *str)
{
	int w, h, x = 0;
	int gw = 0, gh = 0, asc = 0, y0 = 0, xofs = 0, adv = 0;
	int ch;
	
	if (!fb_txt_strsize(f, size, str, &w, &h)) return NULL;
	
	fb_image *res = fb_img_new(w, h, 1);
	if (!res) return NULL;
	uint8_t *buf = res->mem;
	
	char* xstr = (char*) str;
	float scale = stbtt_ScaleForPixelHeight(&f->info, size);
	while ((ch = utf8NextChar(&xstr))) {
		if (ch == ' ') {
			x += f->spacewidth * scale;
		} else {
			int g = _getcharinfo(f, size, ch, &gw, &gh, &asc, &y0, &xofs, &adv);
			stbtt_MakeGlyphBitmap(&f->info, buf + x + xofs + (asc + y0) * w, gw, gh, w, scale, scale, g);
			x += adv + xofs;
		}
	}
	
	return res;	
}

int fb_txt_render(fb_image *img, uint32_t col, fb_font *f, int size, const char *str, int16_t x, int16_t y)
{
	fb_image *mask = fb_txt_render_alpha(f, size, str);
	if (!mask) return 0;
	fb_img_blit_color_alpha(col, img->ch, mask, img, x, y);
	fb_img_free(mask);
	return 1;
}

int fb_txt_print(fb_framebuf *fb, uint32_t col, uint32_t ch, fb_font *f, int size, const char *str, int16_t x, int16_t y)
{
	int w, h;
	if (!fb_txt_strsize(f, size, str, &w, &h))
		return 0;
	fb_image *tmp = fb_img_new(w, h, ch);
	if (!fb_txt_render(tmp, col, f, size, str, 0, 0))
		return 0;
	fb_blit(tmp, fb, x, y);
	fb_img_free(tmp);
	return 1;
}

int fb_txt_strsize(fb_font *f, int size, const char *str, int *w, int *h)
{
	if (!str || !*str) {
		*w = *h = 0;
		return 0;
	}
	
	int ch, width = 0;
	int gw = 0, gh = 0, asc = 0, ybase = 0, xofs = 0, adv = 0;
	float scale = stbtt_ScaleForPixelHeight(&f->info, size);
	
	char* xstr = (char*) str;
	while ((ch = utf8NextChar(&xstr))) {
		if (ch == ' ') {
			width += f->spacewidth * scale;
		} else {
			_getcharinfo(f, size, ch, &gw, &gh, &asc, &ybase, &xofs, &adv);
			width += adv + xofs;
		}
	}
	*w = width;
	*h = size;
	return 1;
}
