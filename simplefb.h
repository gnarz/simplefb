/* simplefb.c
 *
 * provide simple image blitting and stuff for framebuffers
 *
 * Gunnar Zötl <gz@tset.de>, 2014
 * Licensed under the terms of the MIT license. See file LICENSE for details.
 */

#ifndef libfb_h
#define libfb_h

const char* fb_geterrstr();

#include <stdint.h>
#include <linux/fb.h>

typedef struct _fb_framebuffer {
	int fd;
	void *mem;
	struct fb_fix_screeninfo fix;
	struct fb_var_screeninfo var;
} fb_framebuf;

/* return framebuffer width, in pixels */
#define fb_framebuffer_width(fb) ((fb)->var.xres)
/* return framebuffer height, im pixels */
#define fb_framebuffer_height(fb) ((fb)->var.yres)

typedef struct _fb_image {
	uint8_t* mem;
	uint32_t ch, w, h;
} fb_image;

/* return image width, in pixels
 */
#define fb_img_width(img) ((img)->w)

/* return image height, in pixels
 */
#define fb_img_height(img) ((img)->h)

/* return image depth, 1 to 4 bytes.
 */
#define fb_img_depth(img) ((img)->ch)

/* get r, g, b, a components from an image color with ch channels
 */
static inline void fb_img_getrgba(uint32_t col, uint32_t ch, uint32_t *r, uint32_t *g, uint32_t *b, uint32_t *a)
{
	switch (ch) {
		case 1: /* grey only */
			*r = *g = *b = (col & 0x000000FF);
			*a = 0xFF;
			break;
#ifdef BIGENDIAN
		case 2: /* grey + alpha */
			*r = *g = *b = ((col & 0x0000FF00) >> 8);
			*a = (col & 0x000000FF);
			break;
		case 3: /* rgb */
			*r = (col & 0x00FF0000) >> 16;
			*g = (col & 0x0000FF00) >> 8;
			*b = (col & 0x000000FF);
			*a = 0xFF;
			break;
		default: /* rgba */
			*r = (col & 0xFF000000) >> 24;
			*g = (col & 0x00FF0000) >> 16;
			*b = (col & 0x0000FF00) >> 8;
			*a = (col & 0x000000FF);
			break;
#else
		case 2: /* grey + alpha */
			*r = *g = *b = ((col & 0x000000FF));
			*a = (col & 0x0000FF00) >> 8;
			break;
		case 3: /* rgb */
			*b = (col & 0x00FF0000) >> 16;
			*g = (col & 0x0000FF00) >> 8;
			*r = (col & 0x000000FF);
			*a = 0xFF;
			break;
		default: /* rgba */
			*a = (col & 0xFF000000) >> 24;
			*b = (col & 0x00FF0000) >> 16;
			*g = (col & 0x0000FF00) >> 8;
			*r = (col & 0x000000FF);
			break;
#endif
	}
}

/* makes an image color with n channels from r, g, b, a components
 */
static inline uint32_t fb_img_mkcolor(uint32_t ch, uint32_t r, uint32_t g, uint32_t b, uint32_t a)
{
	switch (ch) {
		case 1: /* grey only only */
			return (r+g+b) / 3 & 0xFF;
#ifdef BIGENDIAN
		case 2: /* grey + alpha */
			r &= 0xFF; g &= 0xFF; b &= 0xFF; a &= 0xFF;
			return ((r+g+b) / 3 << 8) | a;
		case 3: /* rgb */
			r &= 0xFF; g &= 0xFF; b &= 0xFF;
			return (r << 16) | (g << 8) | b;
		default: /* rgba */
			r &= 0xFF; g &= 0xFF; b &= 0xFF; a &= 0xFF;
			return (r << 24) | (g << 16) | (b << 8) | a;
#else
		case 2: /* grey + alpha */
			r &= 0xFF; g &= 0xFF; b &= 0xFF; a &= 0xFF;
			return ((r+g+b) / 3) | (a << 8);
		case 3: /* rgb */
			r &= 0xFF; g &= 0xFF; b &= 0xFF;
			return (b << 16) | (g << 8) | r;
		default: /* rgba */
			r &= 0xFF; g &= 0xFF; b &= 0xFF; a &= 0xFF;
			return (a << 24) | (b << 16) | (g << 8) | r;
#endif
	}
	return 0;
}

/* converts an image color with ch channels to a color value suitable to
 * use with framebuffer fb
 */
static inline uint32_t fb_cvtcolor(fb_framebuf* fb, uint32_t col, uint32_t ch)
{
	/* endianness dependent? */
	uint32_t r, g, b, a;
	fb_img_getrgba(col, ch, &r, &g, &b, &a);
	uint32_t p_out = ((a >> (8 - fb->var.transp.length)) & 0xFF) << fb->var.transp.offset;
	p_out |= ((b >> (8 - fb->var.blue.length)) & 0xFF) << fb->var.blue.offset;
	p_out |= ((g >> (8 - fb->var.green.length)) & 0xFF) << fb->var.green.offset;
	p_out |= ((r >> (8 - fb->var.red.length)) & 0xFF) << fb->var.red.offset;
	return p_out;
}

/* convert an image color with ch_in channels to an image color with
 * ch_out channels.
 */
static inline uint32_t fb_img_cvtcolor(uint32_t col, uint32_t ch_in, uint32_t ch_out)
{
	if (ch_in == ch_out) return col;

	uint32_t r, g, b, a;
	fb_img_getrgba(col, ch_in, &r, &g, &b, &a);
	return fb_img_mkcolor(ch_out, r, g, b, a);
}

/* gets the color of the image pixel pointed to by p with ch channels
 */
static inline uint32_t fb_img_getpixel_at(uint8_t *p, uint32_t ch)
{
	switch (ch) {
		case 4: return *((uint32_t*)p);
		case 2: return *((uint16_t*)p) & 0xFFFF;
		case 1: return *p & 0xFF;
#ifdef BIGENDIAN
		case 3: return (*p << 16) | (p[1] << 8) | p[2];
#else
		case 3: return *p | (p[1] << 8) | (p[2] << 16);
#endif
	}
	return 0;
}

/* sets the color of the image pixel pointed to by p to col with ch
 * channels
 */
static inline void fb_img_setpixel_at(uint8_t *p, uint32_t ch, uint32_t col)
{
	switch (ch) {
		case 4: *((uint32_t*)p) = col; break;
		case 2: *((uint16_t*)p) = col & 0xFFFF; break;
		case 1: *p = col & 0xFF; break;
		case 3: {
			uint32_t r, g, b, a;
			fb_img_getrgba(col, ch, &r, &g, &b, &a);
			*p = r;
			p[1] = g;
			p[2] = b;
		}
	}
}

/*** framebuffer functions ***/

/* open a framebuffer device. Returns 0 on failure, a pointer to a
 * fb_framebuf struct on success.
 */
fb_framebuf* fb_open(const char* fb_name);

/* close a framebuffer device and free() associated data. Returns 1
 * on success, 0 on failure. See source for additional details.
 */
int fb_close(fb_framebuf *fb);

/* clear a framebuffer to a color. See fb_cvtcolor() above.
 */
void fb_clear(fb_framebuf *fb, uint32_t col);

/* sets a pixel in the framebuffer to a color. See fb_cvtcolor() above.
 */
void fb_setpixel(fb_framebuf *fb, uint32_t x, uint32_t y, uint32_t col);

/* blit an image to a framebuffer at position xofs, yofs
 */
int fb_blit(fb_image *img, fb_framebuf *fb, int xofs, int yofs);

/*** image functions ***/

/* create a new image with the given dimensions and ch channels per
 * pixel. ch must be from 1 to 4
 */
fb_image *fb_img_new(uint32_t w, uint32_t h, uint32_t ch);

/* loads an image from disc, forcing the resulting image to wantch
 * channels if wantch != 0. wantch must be 0..4
 */
fb_image *fb_img_load(const char* name, int wantch);

/* convert image to another pixel depth. ch must be 1..4
 */
fb_image *fb_img_convert(fb_image *img, uint32_t ch);

/* free()s an image
 */
void fb_img_free(fb_image* img);

/* sets a pixel in an image to a color. See fb_img_mkcolor() above.
 */
void fb_img_setpixel(fb_image *img, uint32_t x, uint32_t y, uint32_t col);

/* sets a pixel in an image to a color alpha blended with the background.
 * See fb_img_mkcolor() above.
 */
void fb_img_setpixel_alpha(fb_image *img, uint32_t x, uint32_t y, uint32_t col);

/* gets the color of a pixel from an image. See fb_img_getrgba() above.
 */
uint32_t fb_img_getpixel(fb_image *img, uint32_t x, uint32_t y);

/* clears an image to a color. See fb_img_mkcolor() above.
 */
void fb_img_clear(fb_image *img, uint32_t col);

/* blits image img into image target at position xofs, yofs.
 */
int fb_img_blit(fb_image *img, fb_image *target, int xofs, int yofs);

/* blits image img into image target at position xofs, yofs with alpha
 * blending, i.e. new color if (alpha * new color) + ((1-alpha)*old color)
 * The old a component is not taken into account.
 */
int fb_img_blit_alpha(fb_image *img, fb_image *target, int xofs, int yofs);

/* blits an alpha mask into an image, using image color col with ch
 * channels as foreground color.
 */
int fb_img_blit_color_alpha(uint32_t col, uint32_t ch, fb_image *mask, fb_image *target, int xofs, int yofs);

/*** font and text handling ***/

typedef struct _fb_font fb_font;

/* load a font. Returns 0 on failure, or else a pointer to a fb_font
 * structure
 */
fb_font *fb_txt_loadfont(const char *name);

/* unloads a font ans free()s associated resources.
 */
void fb_txt_freefont(fb_font *f);

/* returns a fonts name.
 */
const char* fb_txt_getfontname(fb_font *f);

/* render a string to an alpha map (=1 byte-per-pixel image). Returns
 * the newly created image, or 0 on failure.
 */
fb_image *fb_txt_render_alpha(fb_font *f, int size, const char *str);

/* render a string at position x, y to image img, using font f in size
 * size and color col.
 */
int fb_txt_render(fb_image *img, uint32_t col, fb_font *f, int size, const char *str, int16_t x, int16_t y);

/* prints a string directly to the frame buffer fb at position x, y,
 * using color col (ch channels), font f, size size. Returns 0 on failure,
 * 1 otherwise.
 */
int fb_txt_print(fb_framebuf *fb, uint32_t col, uint32_t ch, fb_font *f, int size, const char *str, int16_t x, int16_t y);

/* returns the dimensions of the string str rendered in font f at size
 * size. On failure, returns 0 and w and h are unchanged, on success
 * returns 1 and w and h are width and height of the rendered string.
 */
int fb_txt_strsize(fb_font *f, int size, const char *str, int *w, int *h);

#endif /* libfb_h */
