/* lsimplefb.c
 *
 * provide simple image blitting and stuff for framebuffers as a lua module
 *
 * Gunnar Zötl <gz@tset.de>, 2014
 * Licensed under the terms of the MIT license. See file LICENSE for details.
 */

#include <string.h>

#include "lua.h"
#include "lauxlib.h"

#include "simplefb.h"

#define VERSION 0.1
#define FB_FRAMEBUF "fb_framebuf"
#define FB_IMAGE "fb_image"
#define FB_FONT "fb_font"

#define BUFSIZE 256

static int fbL_error(lua_State *L)
{
	lua_pushnil(L);
	const char* why = fb_geterrstr();
	lua_pushstring(L, why ? why : "(unknown)");
	return 2;
}

/*** framebuffer ***/

typedef struct _fbL_framebuf {
	fb_framebuf *fb;
} fbL_framebuf;

static fbL_framebuf* fbL_toFramebuf(lua_State *L, int index)
{
	fbL_framebuf *sfb = (fbL_framebuf*) lua_touserdata(L, index);
	return sfb;
}

static fbL_framebuf* fbL_checkFramebuf(lua_State *L, int index)
{
	fbL_framebuf *sfb = (fbL_framebuf*) luaL_checkudata(L, index, FB_FRAMEBUF);
	return sfb;
}

static fbL_framebuf* fbL_pushFramebuf(lua_State *L)
{
	fbL_framebuf *sfb = (fbL_framebuf*) lua_newuserdata(L, sizeof(fbL_framebuf));
	sfb->fb = NULL;
	luaL_getmetatable(L, FB_FRAMEBUF);
	lua_setmetatable(L, -2);
	return sfb;
}

static int fbL__Framebufgc(lua_State *L)
{
	fbL_framebuf *sfb = fbL_toFramebuf(L, 1);
	if (sfb->fb) fb_close(sfb->fb);
	return 0;
}

static int fbL__FramebuftoString(lua_State *L)
{
	fbL_framebuf *sfb = fbL_checkFramebuf(L, 1);
	char buf[BUFSIZE];
	snprintf(buf, BUFSIZE, "%s: %p", FB_FRAMEBUF, sfb);
	lua_pushstring(L, buf);
	return 1;
}

static int fbL__Framebufindex(lua_State *L)
{
	fbL_framebuf *sfb = fbL_checkFramebuf(L, 1);
	char *what = (char*)luaL_checkstring(L, 2);
	if (!strcmp(what, "width"))
		lua_pushinteger(L, fb_framebuffer_width(sfb->fb));
	else if (!strcmp(what, "height"))
		lua_pushinteger(L, fb_framebuffer_height(sfb->fb));
	else
		lua_pushnil(L);
	return 1;
}

static const luaL_Reg fbL_framebufmeta[] = {
	{"__gc", fbL__Framebufgc},
	{"__tostring", fbL__FramebuftoString},
	{"__index", fbL__Framebufindex},
	{0, 0}
};

/* int fb_close(fb_framebuf *fb) */
static int fbL_fb_open(lua_State *L)
{
	const char *which = luaL_checkstring(L, 1);
	fb_framebuf *fb = fb_open(which);
	if (fb) {
		fbL_framebuf *sfb = fbL_pushFramebuf(L);
		sfb->fb = fb;
		return 1;
	}
	return fbL_error(L);
}

/* void fb_clear(fb_framebuf *fb, uint32_t col) */
static int fbL_fb_clear(lua_State *L)
{
	fbL_framebuf *sfb = fbL_checkFramebuf(L, 1);
	uint32_t col = luaL_checkunsigned(L, 2);
	fb_clear(sfb->fb, col);
	return 0;
}

/* void fb_setpixel(fb_framebuf *fb, uint32_t x, uint32_t y, uint32_t col) */
static int fbL_fb_setpixel(lua_State *L)
{
	fbL_framebuf *sfb = fbL_checkFramebuf(L, 1);
	int x = luaL_checkinteger(L, 2);
	int y = luaL_checkinteger(L, 3);
	uint32_t col = luaL_checkunsigned(L, 4);
	fb_setpixel(sfb->fb, x, y, col);
	return 0;
}

/* static inline uint32_t fb_img_cvtcolor(uint32_t col, uint32_t ch_in, uint32_t ch_out) */
static int fbL_fb_cvtcolor(lua_State *L)
{
	fbL_framebuf *sfb = fbL_checkFramebuf(L, 1);
	uint32_t col = luaL_checkunsigned(L, 2);
	uint32_t ch = luaL_checkunsigned(L, 3);
	lua_pushunsigned(L, fb_cvtcolor(sfb->fb, col, ch));
	return 1;
}

/*** images ***/

typedef struct _fbL_image {
	fb_image *img;
} fbL_image;

static fbL_image* fbL_toImage(lua_State *L, int index)
{
	fbL_image *simg = (fbL_image*) lua_touserdata(L, index);
	return simg;
}

static fbL_image* fbL_checkImage(lua_State *L, int index)
{
	fbL_image *simg = (fbL_image*) luaL_checkudata(L, index, FB_IMAGE);
	return simg;
}

static fbL_image* fbL_pushImage(lua_State *L)
{
	fbL_image *simg = (fbL_image*) lua_newuserdata(L, sizeof(fbL_image));
	simg->img = NULL;
	luaL_getmetatable(L, FB_IMAGE);
	lua_setmetatable(L, -2);
	return simg;
}

static int fbL__Imagegc(lua_State *L)
{
	fbL_image *simg = fbL_toImage(L, 1);
	if (simg->img) fb_img_free(simg->img);
	return 0;
}

static int fbL__ImagetoString(lua_State *L)
{
	fbL_image *simg = fbL_checkImage(L, 1);
	char buf[BUFSIZE];
	snprintf(buf, BUFSIZE, "%s: %p", FB_IMAGE, simg);
	lua_pushstring(L, buf);
	return 1;
}

static int fbL__Imageindex(lua_State *L)
{
	fbL_image *simg = fbL_checkImage(L, 1);
	char *what = (char*)luaL_checkstring(L, 2);
	if (!strcmp(what, "width"))
		lua_pushinteger(L, fb_img_width(simg->img));
	else if (!strcmp(what, "height"))
		lua_pushinteger(L, fb_img_height(simg->img));
	else if (!strcmp(what, "depth"))
		lua_pushinteger(L, fb_img_depth(simg->img));
	else
		lua_pushnil(L);
	return 1;
}

static const luaL_Reg fbL_imagemeta[] = {
	{"__gc", fbL__Imagegc},
	{"__tostring", fbL__ImagetoString},
	{"__index", fbL__Imageindex},
	{0, 0}
};

/* static inline void fb_img_getrgba(uint32_t col, uint32_t ch, uint32_t *r, uint32_t *g, uint32_t *b, uint32_t *a) */
static int fbL_img_getrgba(lua_State *L)
{
	uint32_t col = luaL_checkunsigned(L, 1);
	uint32_t ch = luaL_checkunsigned(L, 2);
	uint32_t r, g, b, a;
	fb_img_getrgba(col, ch, &r, &g, &b, &a);
	lua_pushunsigned(L, r);
	lua_pushunsigned(L, g);
	lua_pushunsigned(L, b);
	lua_pushunsigned(L, a);
	return 4;
}

/* static inline uint32_t fb_img_mkcolor(uint32_t ch, uint32_t r, uint32_t g, uint32_t b, uint32_t a) */
static int fbL_img_mkcolor(lua_State *L)
{
	uint32_t ch = luaL_checkunsigned(L, 1);
	uint32_t r = luaL_checkunsigned(L, 2);
	uint32_t g = luaL_checkunsigned(L, 3);
	uint32_t b = luaL_checkunsigned(L, 4);
	uint32_t a = luaL_optunsigned(L, 5, 255);
	lua_pushunsigned(L, fb_img_mkcolor(ch, r, g, b, a));
	return 1;
}

/* static inline uint32_t fb_img_cvtcolor(uint32_t col, uint32_t ch_in, uint32_t ch_out) */
static int fbL_img_cvtcolor(lua_State *L)
{
	uint32_t col = luaL_checkunsigned(L, 1);
	uint32_t ch_in = luaL_checkunsigned(L, 2);
	uint32_t ch_out = luaL_checkunsigned(L, 3);
	lua_pushunsigned(L, fb_img_cvtcolor(col, ch_in, ch_out));
	return 1;
}

/* fb_image *fb_img_new(uint32_t w, uint32_t h, uint32_t ch) */
static int fbL_img_new(lua_State *L)
{
	uint32_t w = luaL_checkunsigned(L, 1);
	uint32_t h = luaL_checkunsigned(L, 2);
	uint32_t ch = luaL_optunsigned(L, 3, 4);
	fb_image *img = fb_img_new(w, h, ch);
	if (img) {
		fbL_image *simg = fbL_pushImage(L);
		simg->img = img;
		return 1;
	}
	return fbL_error(L);
}

/* fb_image *fb_img_load(const char* name, int wantch) */
static int fbL_img_load(lua_State *L)
{
	const char* name = luaL_checkstring(L, 1);
	uint32_t wantch = luaL_optunsigned(L, 2, 0);
	fb_image *img = fb_img_load(name, wantch);
	if (img) {
		fbL_image *simg = fbL_pushImage(L);
		simg->img = img;
		return 1;
	}
	return fbL_error(L);
}

/* fb_image *fb_img_convert(fb_image *img, uint32_t ch) */
static int fbL_img_convert(lua_State *L)
{
	fbL_image *simg = fbL_checkImage(L, 1);
	uint32_t wantch = luaL_optunsigned(L, 2, 0);
	fb_image *img = fb_img_convert(simg->img, wantch);
	if (img) {
		fbL_image *simg = fbL_pushImage(L);
		simg->img = img;
		return 1;
	}
	return fbL_error(L);
}

/* void fb_img_setpixel(fb_image *img, uint32_t x, uint32_t y, uint32_t col) */
static int fbL_img_setpixel(lua_State *L)
{
	fbL_image *simg = fbL_checkImage(L, 1);
	uint32_t x = luaL_checkunsigned(L, 2);
	uint32_t y = luaL_checkunsigned(L, 3);
	uint32_t col = luaL_checkunsigned(L, 4);
	fb_img_setpixel(simg->img, x, y, col);
	return 0;
}

/* void fb_img_setpixel_alpha(fb_image *img, uint32_t x, uint32_t y, uint32_t col) */
static int fbL_img_setpixel_alpha(lua_State *L)
{
	fbL_image *simg = fbL_checkImage(L, 1);
	uint32_t x = luaL_checkunsigned(L, 2);
	uint32_t y = luaL_checkunsigned(L, 3);
	uint32_t col = luaL_checkunsigned(L, 4);
	fb_img_setpixel_alpha(simg->img, x, y, col);
	return 0;
}

/* uint32_t fb_img_getpixel(fb_image *img, uint32_t x, uint32_t y) */
static int fbL_img_getpixel(lua_State *L)
{
	fbL_image *simg = fbL_checkImage(L, 1);
	uint32_t x = luaL_checkunsigned(L, 2);
	uint32_t y = luaL_checkunsigned(L, 3);
	lua_pushunsigned(L, fb_img_getpixel(simg->img, x, y));
	return 1;
}

/* void fb_img_clear(fb_image *img, uint32_t col) */
static int fbL_img_clear(lua_State *L)
{
	fbL_image *simg = fbL_checkImage(L, 1);
	uint32_t col = luaL_checkunsigned(L, 2);
	fb_img_clear(simg->img, col);
	return 0;
}

/* int fb_img_blit(fb_image *img, fb_image *target, int xofs, int yofs) */
static int fbL_img_blit(lua_State *L)
{
	fbL_image *simg = fbL_checkImage(L, 1);
	fbL_image *starg = fbL_checkImage(L, 2);
	uint32_t x = luaL_checkunsigned(L, 3);
	uint32_t y = luaL_checkunsigned(L, 4);
	lua_pushboolean(L, fb_img_blit(simg->img, starg->img, x, y));
	return 1;
}

/* int fb_img_blit_alpha(fb_image *img, fb_image *target, int xofs, int yofs) */
static int fbL_img_blit_alpha(lua_State *L)
{
	fbL_image *simg = fbL_checkImage(L, 1);
	fbL_image *starg = fbL_checkImage(L, 2);
	uint32_t x = luaL_checkunsigned(L, 3);
	uint32_t y = luaL_checkunsigned(L, 4);
	lua_pushboolean(L, fb_img_blit_alpha(simg->img, starg->img, x, y));
	return 1;
}

/* int fb_img_blit_color_alpha(uint32_t col, uint32_t ch, fb_image *mask, fb_image *target, int xofs, int yofs) */
static int fbL_img_blit_color_alpha(lua_State *L)
{
	uint32_t col = luaL_checkunsigned(L, 1);
	uint32_t ch = luaL_checkunsigned(L, 2);
	fbL_image *smask = fbL_checkImage(L, 3);
	fbL_image *starg = fbL_checkImage(L, 4);
	uint32_t x = luaL_checkunsigned(L, 5);
	uint32_t y = luaL_checkunsigned(L, 6);
	lua_pushboolean(L, fb_img_blit_color_alpha(col, ch, smask->img, starg->img, x, y));
	return 1;
}

/* int fb_blit(fb_image *img, fb_framebuf *fb, int xofs, int yofs) */
static int fbL_fb_blit(lua_State *L)
{
	fbL_image *simg = fbL_checkImage(L, 1);
	fbL_framebuf *sfb = fbL_checkFramebuf(L, 2);
	int x = luaL_optinteger(L, 3, 0);
	int y = luaL_optinteger(L, 4, 0);
	lua_pushboolean(L, fb_blit(simg->img, sfb->fb, x, y));
	return 1;
}

/*** fonts and text ***/

typedef struct _fbL_font {
	fb_font *fnt;
} fbL_font;

static fbL_font* fbL_toFont(lua_State *L, int index)
{
	fbL_font *sfnt = (fbL_font*) lua_touserdata(L, index);
	return sfnt;
}

static fbL_font* fbL_checkFont(lua_State *L, int index)
{
	fbL_font *sfnt = (fbL_font*) luaL_checkudata(L, index, FB_FONT);
	return sfnt;
}

static fbL_font* fbL_pushFont(lua_State *L)
{
	fbL_font *sfnt = (fbL_font*) lua_newuserdata(L, sizeof(fbL_font));
	sfnt->fnt = NULL;
	luaL_getmetatable(L, FB_FONT);
	lua_setmetatable(L, -2);
	return sfnt;
}

static int fbL__Fontgc(lua_State *L)
{
	fbL_font *sfnt = fbL_toFont(L, 1);
	if (sfnt->fnt) fb_txt_freefont(sfnt->fnt);
	return 0;
}

static int fbL__FonttoString(lua_State *L)
{
	fbL_font *sfnt = fbL_checkFont(L, 1);
	char buf[BUFSIZE];
	snprintf(buf, BUFSIZE, "%s: %p", FB_FONT, sfnt);
	lua_pushstring(L, buf);
	return 1;
}

static int fbL__Fontindex(lua_State *L)
{
	fbL_font *sfnt = fbL_checkFont(L, 1);
	char *what = (char*)luaL_checkstring(L, 2);
	if (!strcmp(what, "name"))
		lua_pushstring(L, fb_txt_getfontname(sfnt->fnt));
	else
		lua_pushnil(L);
	return 1;
}

/* fb_font *fb_txt_loadfont(const char *name) */
static int fbL_txt_loadfont(lua_State *L)
{
	const char *name = luaL_checkstring(L, 1);
	fb_font *fnt = fb_txt_loadfont(name);
	if (fnt) {
		fbL_font *sfnt = fbL_pushFont(L);
		sfnt->fnt = fnt;
		return 1;
	}
	return fbL_error(L);
}

/* fb_image *fb_txt_render_alpha(fb_font *f, int size, const char *str) */
static int fbL_txt_render_alpha(lua_State *L)
{
	fbL_font *sfnt = fbL_checkFont(L, 1);
	int size = luaL_checkinteger(L, 2);
	const char* str = luaL_checkstring(L, 3);
	fb_image *img = fb_txt_render_alpha(sfnt->fnt, size, str);
	if (img) {
		fbL_image *simg = fbL_pushImage(L);
		simg->img = img;
		return 1;
	}
	return fbL_error(L);
}

/* int fb_txt_render(fb_image *img, uint32_t col, fb_font *f, int size, const char *str, int16_t x, int16_t y) */
static int fbL_txt_render(lua_State *L)
{
	fbL_image *simg = fbL_checkImage(L, 1);
	uint32_t col = luaL_checkunsigned(L, 2);
	fbL_font *sfnt = fbL_checkFont(L, 3);
	int size = luaL_checkinteger(L, 4);
	const char* str = luaL_checkstring(L, 5);
	int x = luaL_checkinteger(L, 6);
	int y = luaL_checkinteger(L, 7);
	lua_pushboolean(L, fb_txt_render(simg->img, col, sfnt->fnt, size, str, x, y));
	return 1;
}

/* int fb_txt_print(fb_framebuf *fb, uint32_t col, uint32_t ch, fb_font *f, int size, const char *str, int16_t x, int16_t y) */
static int fbL_txt_print(lua_State *L)
{
	fbL_framebuf *sfb = fbL_checkFramebuf(L, 1);
	uint32_t col = luaL_checkunsigned(L, 2);
	uint32_t ch = luaL_checkunsigned(L, 3);
	fbL_font *sfnt = fbL_checkFont(L, 4);
	int size = luaL_checkinteger(L, 5);
	const char* str = luaL_checkstring(L, 6);
	int x = luaL_checkinteger(L, 7);
	int y = luaL_checkinteger(L, 8);
	lua_pushboolean(L, fb_txt_print(sfb->fb, col, ch, sfnt->fnt, size, str, x, y));
	return 1;
}

/* int fb_txt_strsize(fb_font *f, int size, const char *str, int *w, int *h) */
static int fbL_txt_strsize(lua_State *L)
{
	fbL_font *sfnt = fbL_checkFont(L, 1);
	int size = luaL_checkinteger(L, 2);
	const char* str = luaL_checkstring(L, 3);
	int w, h;
	fb_txt_strsize(sfnt->fnt, size, str, &w, &h);
	lua_pushinteger(L, w);
	lua_pushinteger(L, h);
	return 2;
}

static const luaL_Reg fbL_fontmeta[] = {
	{"__gc", fbL__Fontgc},
	{"__tostring", fbL__FonttoString},
	{"__index", fbL__Fontindex},
	{0, 0}
};


static const struct luaL_Reg fbL_functions [] ={
	{"fb_open", fbL_fb_open},
	{"fb_clear", fbL_fb_clear},
	{"fb_setpixel", fbL_fb_setpixel},
	{"fb_cvtcolor", fbL_fb_cvtcolor},
	{"fb_blit", fbL_fb_blit},
	
	{"img_getrgba", fbL_img_getrgba},
	{"img_mkcolor", fbL_img_mkcolor},
	{"img_cvtcolor", fbL_img_cvtcolor},
	{"img_new", fbL_img_new},
	{"img_load", fbL_img_load},
	{"img_convert", fbL_img_convert},
	{"img_setpixel", fbL_img_setpixel},
	{"img_setpixel_alpha", fbL_img_setpixel_alpha},
	{"img_getpixel", fbL_img_getpixel},
	{"img_clear", fbL_img_clear},
	{"img_blit", fbL_img_blit},
	{"img_blit_alpha", fbL_img_blit_alpha},
	{"img_blit_color_alpha", fbL_img_blit_color_alpha},
	
	{"txt_loadfont", fbL_txt_loadfont},
	{"txt_render_alpha", fbL_txt_render_alpha},
	{"txt_render", fbL_txt_render},
	{"txt_print", fbL_txt_print},
	{"txt_strsize", fbL_txt_strsize},

	{NULL, NULL}
};

/* luaopen_gfx
 * 
 * open and initialize this library
 */
int luaopen_simplefb(lua_State *L)
{
	luaL_newlib(L, fbL_functions);

	lua_pushliteral(L, "_VERSION");
	lua_pushnumber(L, VERSION);
	lua_rawset(L, -3);

	/* add framebuffer userdata metatable */
	luaL_newmetatable(L, FB_FRAMEBUF);
	luaL_setfuncs(L, fbL_framebufmeta, 0);
	lua_pop(L, 1);

	/* add image userdata metatable */
	luaL_newmetatable(L, FB_IMAGE);
	luaL_setfuncs(L, fbL_imagemeta, 0);
	lua_pop(L, 1);

	/* add font userdata metatable */
	luaL_newmetatable(L, FB_FONT);
	luaL_setfuncs(L, fbL_fontmeta, 0);
	lua_pop(L, 1);

	return 1;
}
