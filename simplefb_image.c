/* simplefb_image.c
 *
 * provide simple image blitting and manipulating
 *
 * Gunnar Zötl <gz@tset.de>, 2014
 * Licensed under the terms of the MIT license. See file LICENSE for details.
 */

#include <stdlib.h>
#include <stdint.h>
#include "simplefb.h"
#include "stb_image.h"

#include <stdio.h>

extern void fb_seterrstr(const char* str, ...);

fb_image *fb_img_new(uint32_t w, uint32_t h, uint32_t ch)
{
	if (ch > 4) ch = 4; else if (ch < 1) ch = 1;
	fb_image *img = calloc(1, sizeof(fb_image));
	if (!img) return img;
	img->w = w;
	img->h = h;
	img->ch = ch;
	img->mem = calloc(w * h, ch);
	if (!img->mem) {
		free(img);
		img = 0;
	}
	return img;
}

fb_image *fb_img_load(const char* name, int wantch)
{
	int w, h, ch;
	if (wantch > 4) wantch = 4; else if (wantch < 0) wantch = 0;
	uint8_t *data = stbi_load(name, &w, &h, &ch, wantch);
	if (!data) {
		fb_seterrstr("image load failed: %s\n", stbi_failure_reason());
		return 0;
	}
	fb_image *res = calloc(1, sizeof(fb_image));
	res->w = w;
	res->h = h;
	res->ch = wantch != 0 ? wantch : ch;
	res->mem = data;
	return res;
}

fb_image *fb_img_convert(fb_image *img, uint32_t tch)
{
	fb_image *res = fb_img_new(img->w, img->h, tch);
	tch = res->ch;
	uint8_t *s = img->mem, *t = res->mem;
	uint32_t x, y, sch = img->ch;
	for (y = 0; y < img->h; ++y) {
		for (x = 0; x < img->w; ++x) {
			uint32_t col = fb_img_getpixel_at(s, sch);
			if (sch != tch) col = fb_img_cvtcolor(col, sch, tch);
			fb_img_setpixel_at(t, tch, col);
			s += sch;
			t += tch;
		}
	}
	return res;
}

void fb_img_free(fb_image* img)
{
	if (img->mem) free(img->mem);
	free(img);
}

void fb_img_setpixel(fb_image *img, uint32_t x, uint32_t y, uint32_t col)
{
	uint8_t *p = img->mem + (y * fb_img_width(img) + x) * img->ch;
	fb_img_setpixel_at(p, img->ch, col);
}

void fb_img_setpixel_alpha(fb_image *img, uint32_t x, uint32_t y, uint32_t col)
{
	uint8_t *p = img->mem + (y * fb_img_width(img) + x) * img->ch;
	uint32_t col0 = fb_img_getpixel_at(p, img->ch);
	uint32_t sr, sg, sb, sa, tr, tg, tb, ta;

	fb_img_getrgba(col, img->ch, &sr, &sg, &sb, &sa);
	fb_img_getrgba(col0, img->ch, &tr, &tg, &tb, &ta);

	tr = (((tr * (255 - sa)) + (sr * sa)) / 255) & 0xFF;
	tg = (((tg * (255 - sa)) + (sg * sa)) / 255) & 0xFF;
	tb = (((tb * (255 - sa)) + (sb * sa)) / 255) & 0xFF;
	ta = (((ta * (255 - sa)) + (sa * sa)) / 255) & 0xFF;
	
	fb_img_setpixel_at(p, img->ch, fb_img_mkcolor(img->ch, tr, tg, tb, ta));
}

uint32_t fb_img_getpixel(fb_image *img, uint32_t x, uint32_t y)
{
	uint8_t *p = img->mem + (y * fb_img_width(img) + x) * img->ch;
	return fb_img_getpixel_at(p, img->ch);
}

void fb_img_clear(fb_image *img, uint32_t col)
{
	uint8_t *p = img->mem;
	int x, y, ch = img->ch;
	for (y = 0; y < fb_img_height(img); ++y) {
		for (x = 0; x < fb_img_width(img); ++x) {
			fb_img_setpixel_at(p, ch, col);
			p += ch;
		}
	}
}

int fb_img_blit(fb_image *img, fb_image *target, int xofs, int yofs)
{
	int ixofs = 0, iyofs = 0;
	int iw = img->w, ih = img->h;
	
	if ((xofs > (int)target->w) || (yofs > (int)target->h)) return 0;
	
	if (xofs < 0) { ixofs = -xofs; xofs = 0; }
	if (yofs < 0) { iyofs = -yofs; yofs = 0; }
	if ((ixofs >= iw) || (iyofs >= ih)) return 0;
	
	iw -= ixofs;
	if (xofs + iw > (int)target->w) {
		iw = target->w - xofs;
		if (iw <= 0) return 0;
	}
	ih -= iyofs;
	if (yofs + ih > (int)target->h) {
		ih = target->h - yofs;
		if (ih <= 0) return 0;
	}

	uint32_t x, y, col;
	int sch = img->ch, tch = target->ch;
	uint8_t *s, *t;
	for (y = 0; y < ih; ++y) {
		s = img->mem + (y * img->w + ixofs) * img->ch;
		t = target->mem + ((yofs + y) * target->w + xofs) * target->ch;
		for (x = 0; x < iw; ++x) {
			col = fb_img_getpixel_at(s, img->ch);
			if (sch != tch)
				col = fb_img_cvtcolor(col, sch, tch);
			fb_img_setpixel_at(t, target->ch, col);
			s += sch;
			t += tch;
		}
	}
	return 1;
}


int fb_img_blit_alpha(fb_image *img, fb_image *target, int xofs, int yofs)
{
	int ixofs = 0, iyofs = 0;
	int iw = img->w, ih = img->h;
	
	if ((xofs > (int)target->w) || (yofs > (int)target->h)) return 0;
	
	if (xofs < 0) { ixofs = -xofs; xofs = 0; }
	if (yofs < 0) { iyofs = -yofs; yofs = 0; }
	if ((ixofs >= iw) || (iyofs >= ih)) return 0;
	
	iw -= ixofs;
	if (xofs + iw > (int)target->w) {
		iw = target->w - xofs;
		if (iw <= 0) return 0;
	}
	ih -= iyofs;
	if (yofs + ih > (int)target->h) {
		ih = target->h - yofs;
		if (ih <= 0) return 0;
	}

	uint32_t x, y, cols, colt, sr, sg, sb, sa, tr, tg, tb, ta;
	int sch = img->ch, tch = target->ch;
	uint8_t *s, *t;
	for (y = 0; y < ih; ++y) {
		s = img->mem + (y * img->w + ixofs) * sch;
		t = target->mem + ((yofs + y) * target->w + xofs) * tch;
		for (x = 0; x < iw; ++x) {
			cols = fb_img_getpixel_at(s, sch);
			fb_img_getrgba(cols, sch, &sr, &sg, &sb, &sa);

			if (sa > 0) {
				colt = fb_img_getpixel_at(t, tch);
				fb_img_getrgba(colt, tch, &tr, &tg, &tb, &ta);
				
				tr = (((tr * (255 - sa)) + (sr * sa)) / 255) & 0xFF;
				tg = (((tg * (255 - sa)) + (sg * sa)) / 255) & 0xFF;
				tb = (((tb * (255 - sa)) + (sb * sa)) / 255) & 0xFF;
				ta = (((ta * (255 - sa)) + (sa * sa)) / 255) & 0xFF;
				
				fb_img_setpixel_at(t, tch, fb_img_mkcolor(tch, tr, tg, tb, ta));
			}
			
			s += sch;
			t += tch;
		}
	}
	return 1;
}

int fb_img_blit_color_alpha(uint32_t col, uint32_t ch, fb_image *mask, fb_image *target, int xofs, int yofs)
{
	if (mask->ch != 1) return 0;
	int ixofs = 0, iyofs = 0;
	int iw = mask->w, ih = mask->h;
	
	if ((xofs > (int)target->w) || (yofs > (int)target->h)) return 0;
	
	if (xofs < 0) { ixofs = -xofs; xofs = 0; }
	if (yofs < 0) { iyofs = -yofs; yofs = 0; }
	if ((ixofs >= iw) || (iyofs >= ih)) return 0;
	
	iw -= ixofs;
	if (xofs + iw > (int)target->w) {
		iw = target->w - xofs;
		if (iw <= 0) return 0;
	}
	ih -= iyofs;
	if (yofs + ih > (int)target->h) {
		ih = target->h - yofs;
		if (ih <= 0) return 0;
	}

	uint32_t x, y, colt, sr, sg, sb, sa, tr, tg, tb, ta;
	int tch = target->ch;
	uint8_t *s, *t;
	fb_img_getrgba(col, ch, &sr, &sg, &sb, &sa);
	for (y = 0; y < ih; ++y) {
		s = mask->mem + y * mask->w + ixofs;
		t = target->mem + ((yofs + y) * target->w + xofs) * target->ch;
		for (x = 0; x < iw; ++x) {
			sa = *s++;

			if (sa > 0) {
				colt = fb_img_getpixel_at(t, tch);
				fb_img_getrgba(colt, tch, &tr, &tg, &tb, &ta);

				tr = (((tr * (255 - sa)) + (sr * sa)) / 255) & 0xFF;
				tg = (((tg * (255 - sa)) + (sg * sa)) / 255) & 0xFF;
				tb = (((tb * (255 - sa)) + (sb * sa)) / 255) & 0xFF;
				ta = (((ta * (255 - sa)) + (sa * sa)) / 255) & 0xFF;
			
				fb_img_setpixel_at(t, tch, fb_img_mkcolor(tch, tr, tg, tb, ta));
			}
			
			t += tch;
		}
	}
	return 1;
}
