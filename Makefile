# simple Gnu-Makefile for lgfx. Works for Linux, MacOS X, probably other unixen
#
# Gunnar Zötl <gz@tset.de>, 2012-2014
# Released under MIT/X11 license. See file LICENSE for details.

# try some automatic discovery
OS = $(shell uname -s)
LUAVERSION = $(shell lua -e "print(string.match(_VERSION, '%d.%d'))")
LUADIR = $(shell dirname `which lua`)
LUAROOT = $(shell dirname "$(LUADIR)")

# installation target dir
LUA_INST_DIR = $(LUAROOT)/lib/lua/$(LUAVERSION)
INST_LIB_DIR = /usr/lib
INST_INC_DIR = /usr/include

# Defaults
CC = gcc
DEBUG=-g -lefence
#DEBUG=-O3
CFLAGS=-fPIC -Wall $(DEBUG)
LDFLAGS=$(DEBUG)
LUA_LDFLAGS=-shared $(LDFLAGS)
XLDFLAGS=-Wl,-E

ifeq ($(OS),Darwin)
	LUA_LDFLAGS=-bundle -undefined dynamic_lookup -all_load $(LDFLAGS)
	XLDFLAGS= #-Wl,-pagezero_size,10000 -Wl,-image_base,100000000
endif

all: libsimplefb.a

libsimplefb.a: simplefb.o simplefb_image.o stb_image.o simplefb_text.o
	ar cr $@ $^
	ranlib $@

%.o: %.c simplefb.h
	$(CC) $(CFLAGS) -c $<

simplefb_image.o: stb_image.h

simplefb_text.o: stb_truetype.h

test: testsimplefb.o libsimplefb.a
	$(CC) $(CFLAGS) -o testsimplefb testsimplefb.o -L. -lsimplefb -lm

luamod: lsimplefb.o libsimplefb.a
	$(CC) $(LUA_LDFLAGS) -o simplefb.so lsimplefb.o -L. -lsimplefb $(XLDFLAGS)

clean:
	rm -f *.o *.a *.so testsimplefb

install:
	@echo $(LUA_INST_DIR)
	@echo $(INST_LIB_DIR)
	@echo $(INST_INC_DIR)
