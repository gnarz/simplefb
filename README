/* simplefb
 *
 * provide simple image blitting and stuff for framebuffers
 *
 * Gunnar Zötl <gz@tset.de>, 2014
 * Licensed under the terms of the MIT license. See file LICENSE for details.
 */

Usage:
------

C: #include "simplefh.h", link with simplefb
Lua: sfb = require "simplefb"

Utilities
---------

C: const char* fb_geterrstr();

	Returns a string describing the last error that occurred. Returns
	NULL if no description is available.
	Lua: error strings are returned as second return value from the
	functions that produce them, if the first return value is nil.

Framebuffer
-----------

C: fb_framebuf* fb_open(const char* fb_name)
Lua: fb = sfb.fb_open(name)

	open a framebuffer device.
	C: Returns 0 on failure, a pointer to a fb_framebuf on success.
	Lua: Returns nil+error on failure, a fb_framebuf object on success.

C: void fb_clear(fb_framebuf *fb, uint32_t col)
Lua: sfb.fb_clear(fb, col)

	clears a framebuffer to a color. The color must be in framebuffer
	color format, see fb_cvtcolor()

C: int fb_close(fb_framebuf *fb);
Lua: -

	close a framebuffer device and free() associated data. Returns 1
	on success, 0 on failure. See source for additional details.
	Lua: This happens automatically when fb object is garbage collected.

C: #define fb_framebuffer_width(fb) ((fb)->var.xres)
Lua: w = fb.width

	return framebuffer width, in pixels

C: #define fb_framebuffer_height(fb) ((fb)->var.yres)
Lua: h = fb.height

	return framebuffer height, in pixels

C: void fb_setpixel(fb_framebuf *fb, uint32_t x, uint32_t y, uint32_t col)
Lua: sfb.fb_setpixel(fb, c, y, col)

	sets a pixel in a framebuffer to a color. The color must be in
	framebuffer color format, see fb_cvtcolor()

C: uint32_t fb_cvtcolor(fb_framebuf* fb, uint32_t col, uint32_t ch)
Lua: col = sfb.fb_cvtcolor(fb, col, ch)

	converts a color from image format with ch channels to framebuffer
	format.

C: int fb_blit(fb_image *img, fb_framebuf *fb, int xofs, int yofs);
Lua: ok = sfb.fb_blit(img, fb, xofs, yofs)

	blit an image to a framebuffer at position xofs, yofs.
	C: Returns 1 on success, 0 if the image was not blit.
	Lua: dito, true or false.

Images
------

Images can have 1 to 4 bytes per pixel:

	Bytes	Data
	1		alpha or grey
	2		grey, alpha
	3		red, green, blue
	4		red, green, blue, alpha

C: fb_image *fb_img_new(uint32_t w, uint32_t h, uint32_t ch);
Lua: img = sfb.img_new(w, h, ch = 4)

	create a new image with the given dimensions and ch channels per
	pixel. ch must be from 1 to 4.
	C: returns NULL on failure, a pointer to an fb_image on success.
	Lua: returns nil on failure, an fb_image object on success. The
	channels argument is optional and defaults to 4.

C: fb_image *fb_img_load(const char* name, int wantch);
Lua: img = sfb.img_load(name, wantch = 0)

	loads an image from disc, forcing the resulting image to wantch
	channels if wantch != 0. wantch must be 0..4.
	C: returns NULL on failure, a pointer to an fb_image containing the
	loaded image data on success.
	Lua: returns nil+error on failure, an fb_image object containing the
	loaded image data on success.

C: fb_image *fb_img_convert(fb_image *img, uint32_t ch);
Lua: img = sfb.img_convert

	convert image to another pixel depth. ch must be 1..4.
	C: returns NULL on failure, a pointer to an fb_image containing the
	converted image data on success.
	Lua: returns nil+error on failure, an fb_image object containing the
	converted image data on success.

C: fb_img_free()
Lua: -

	free()s an image.
	Lua: this happens automatically when the fb_image object is garbage
	collected.

C: #define fb_img_width(img) ((img)->w)
Lua: w = img.width

	return image width, in pixels

C: #define fb_img_height(img) ((img)->h)
Lua: h = img.height

	return image height in pixels

C: #define fb_img_depth(img) ((img)->ch)
Lua: d = img.depth

	return image depth (no. channels) in bytes.

C: void fb_img_getrgba(uint32_t col, uint32_t ch, uint32_t *r, uint32_t *g, uint32_t *b, uint32_t *a)
Lua: r, g, b, a = sfb.img_getrgba(col, ch)

	get r, g, b, a components from an image color with ch channels. If
	the image has 3 channels, a will be 255, if it has 2 channels, r, g,
	b will be grey and a will be alpha, it it has 1 channel, r, g, b,
	will be grey and a will be 255.

C: uint32_t fb_img_mkcolor(uint32_t ch, uint32_t r, uint32_t g, uint32_t b, uint32_t a)
Lua: col = sfb.img_mkcolor(ch, r, g, b, a = 255)

	makes an image color with n channels from r, g, b, a components. If
	the desired format has 1 channel, grey will be (r+g+b)/3. If it has
	2 channels, grey will be as before, alpha will be a. If it has 3
	channels, a is ignored, if it has 4 channels, all 4 values are used.

C: uint32_t fb_img_cvtcolor(uint32_t col, uint32_t ch_in, uint32_t ch_out)
Lua: col = sfb.img_cvtcolor(col, ch_in, ch_out)

	convert an image color with ch_in channels to an image color with
	ch_out channels.

C: void fb_img_setpixel_at(uint8_t *p, uint32_t ch, uint32_t col)
Lua: -

	sets the color of the image pixel pointed to by p to col with ch
	channels

C: void fb_img_setpixel(fb_image *img, uint32_t x, uint32_t y, uint32_t col);
Lua: sfb.img_setpixel(img, x, y, col)

	sets a pixel in an image to a color. The color must be in image color
	format, with as many channels as the image has. See fb_img_mkcolor().

C: void fb_img_setpixel_alpha(fb_image *img, uint32_t x, uint32_t y, uint32_t col);
Lua: sfb.img_setpixel_alpha(8img, x, y, col)

	sets a pixel in an image to a color alpha blended with the background.
	The color must be in image color format, with as many channels as
	the image has. See fb_img_mkcolor().

C: uint32_t fb_img_getpixel_at(uint8_t *p, uint32_t ch)
Lua: -

	gets the color of the image pixel pointed to by p with ch channels

C:uint32_t fb_img_getpixel(fb_image *img, uint32_t x, uint32_t y);
Lua: col = sfb.img_getpixel(img, x, y)

	gets the color of a pixel from an image. The returned color is in
	image color format, with as many channels as the image has. See
	fb_img_getrgba().

C: void fb_img_clear(fb_image *img, uint32_t col);
Lua: sfb.img_clear(img, col)

	clears an image to a color. The color must be in image color format,
	with as many channels as the image has. See fb_img_mkcolor().

C: int fb_img_blit(fb_image *img, fb_image *target, int xofs, int yofs)
Lua: ok = sfb.img_blit(img, target, xofs, yofs)

	blits image img into image target at position xofs, yofs.

C: int fb_img_blit_alpha(fb_image *img, fb_image *target, int xofs, int yofs)
Lua: ok = sfb.img_blit_alpha(img, target, xofs, yofs)

	blits image img into image target at position xofs, yofs with alpha
	blending, i.e. new color is (alpha*new color) + ((1-alpha)*old color)
	The old a component is not taken into account.

C: int fb_img_blit_color_alpha(uint32_t col, uint32_t ch, fb_image *mask, fb_image *target, int xofs, int yofs)
Lua: ok = sfb.img_blit_color_alpha(col, ch, mask, target, xofs, yofs)

	blits an alpha mask into an image, using image color col with ch
	channels as foreground color.

Text / Fonts
------------

C:fb_font *fb_txt_loadfont(const char *name);
Lua: fnt = sfb.txt_loadfont(name)

	load a font.
	C: Returns NULL on failure, or else a pointer to a fb_font.
	Lua: returns nil+error on failure, a fb_font object on success.

C: void fb_txt_freefont(fb_font *f);
Lua: -

	unloads a font ans free()s associated resources.
	Lua: this happens automatically when the fb_font object is garbage
	collected.

C: const char* fb_txt_getfontname(fb_font *f);
Lua: name = fnt.name

	returns a fonts name.

C: fb_image *fb_txt_render_alpha(fb_font *f, int size, const char *str)
Lua: img = sfb.txt_render_alpha(fnt, size, str)

	render a string to an alpha map (=1 byte-per-pixel image). Returns
	the newly created image, or NULL (Lua: nil) on failure.

C: int fb_txt_render(fb_image *img, uint32_t col, fb_font *f, int size, const char *str, int16_t x, int16_t y);
Lua: ok = sfb.txt_render(img, col, fnt, size, str, x, y)

	render a string at position x, y to image img, using font f in size
	size and color col. col is in image color format with the same number
	of channels img has.
	C: returns 1 on success, 0 on failure.
	Lua: dito with true and false.

C: int fb_txt_print(fb_framebuf *fb, uint32_t col, uint32_t ch, fb_font *f, int size, const char *str, int16_t x, int16_t y);
Lua: ok = sfb.txt_print(fb, col, ch, fnt, size, str, x, y)

	prints a string directly to the frame buffer fb at position x, y,
	using color col (image color format, ch channels), font f, size size.
	C: returns 1 on success, 0 on failure.
	Lua: dito with true and false.

int fb_txt_strsize(fb_font *f, int size, const char *str, int *w, int *h);
Lua: w, h = sfb.txt_strsize(fnt, size, str)

	returns the dimensions of the string str rendered in font f at size
	size. 
	C: On failure, returns 0 and w and h are unchanged, on success
	returns 1 and w and h are width and height of the rendered string.
	Lua: On failure, returns nil. On success, returns width and height
	of the rendered string.
