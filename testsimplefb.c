/* testsimplefb.c
 *
 * test simplefb
 *
 * Gunnar Zötl <gz@tset.de>, 2014
 * Licensed under the terms of the MIT license. See file LICENSE for details.
 */

#include <stdio.h>
#include <stdlib.h>
#include "simplefb.h"

#define MINTEST 1

int main(int argc, char** argv)
{
	unsigned int test = MINTEST;

	/* --- init --- */
	fb_framebuf *fb = fb_open("/dev/fb1");
	if (!fb) exit(1);

	fb_image *img = 0;

#if MINTEST <= 1
	{  printf("test %d...\n", test++);
	/* test: fill black */
	fb_clear(fb, fb_cvtcolor(fb, 0, 4));
	getchar();
	
	/* test: fill red */
	fb_clear(fb, fb_cvtcolor(fb, fb_img_mkcolor(4, 255, 0, 0, 255), 4));
	getchar();

	/* test: fill green */
	fb_clear(fb, fb_cvtcolor(fb, fb_img_mkcolor(4, 0, 255, 0, 255), 4));
	getchar();

	/* test: fill blue */
	fb_clear(fb, fb_cvtcolor(fb, fb_img_mkcolor(4, 0, 0, 255, 255), 4));
	getchar();
	}
#endif

#if MINTEST <= 2
	{  printf("test %d...\n", test++);
	/* test: draw a cross */
	int x, y, w, h;
	w = fb_framebuffer_width(fb);
	h = fb_framebuffer_height(fb);
	for (y = 0; y < h; ++y) {
		for (x = 0; x < w; ++x) {
			if (x == y || w - x == y)
				fb_setpixel(fb, x, y, fb_img_mkcolor(4, 255, 255, 255, 255));
			else
				fb_setpixel(fb, x, y, fb_img_mkcolor(4, 0, 0, 0, 255));
		}
	}
	getchar();

	/* test: create an image, draw a cross into it, blit it to framebuffer */
	img = fb_img_new(fb_framebuffer_width(fb), fb_framebuffer_height(fb), 4);
	for (y = 0; y < h; ++y) {
		for (x = 0; x < w; ++x) {
			if (x == y || w - x == y)
				fb_img_setpixel(img, x, y, fb_img_mkcolor(4, 255, 127, 127, 255));
			else
				fb_img_setpixel(img, x, y, 0);
		}
	}
	fb_blit(img, fb, 0, 0);
	getchar();
	}
#endif

#if MINTEST <= 3
	{  printf("test %d...\n", test++);
	/* test: blit image, top and right are offscreen */
	fb_img_clear(img, fb_img_mkcolor(4, 255, 0, 0, 255));
	fb_blit(img, fb, 30, -30);
	getchar();

	/* test: blit image, left and bottom are offscreen */
	fb_img_clear(img, fb_img_mkcolor(4, 0, 255, 0, 255));
	fb_blit(img, fb, -30, 30);
	getchar();

	/* test: blit image, right and bottom are offscreen */
	fb_img_clear(img, fb_img_mkcolor(4, 0, 0, 255, 255));
	fb_blit(img, fb, 30, 40);
	getchar();
	}
#endif

#if MINTEST <= 4
	{  printf("test %d...\n", test++);
	if (img) fb_img_free(img);
	/* test: load then display image in various forced color depths */
	img = fb_img_load("testimg.jpg", 4);
	if (img) {
		fb_blit(img, fb, 0, 0);
	}
	getchar();

	if (img) fb_img_free(img);
	img = fb_img_load("testimg.jpg", 3);
	if (img) {
		fb_blit(img, fb, 0, 0);
	}
	getchar();

	if (img) fb_img_free(img);
	img = fb_img_load("testimg.jpg", 2);
	if (img) {
		fb_blit(img, fb, 0, 0);
	}
	getchar();

	if (img) fb_img_free(img);
	img = fb_img_load("testimg.jpg", 1);
	if (img) {
		fb_blit(img, fb, 0, 0);
	}
	getchar();
	}
#endif

#if MINTEST <= 5
	{  printf("test %d...\n", test++);
	if (img) fb_img_free(img);
	/* test: load then convert image into various color depths */
	img = fb_img_load("testimg.png", 4);
	fb_image *i1 = fb_img_convert(img, 4);
	fb_blit(i1, fb, 0, 0);
	getchar();
	fb_img_free(i1);

	i1 = fb_img_convert(img, 3);
	fb_blit(i1, fb, 0, 0);
	getchar();
	fb_img_free(i1);

	i1 = fb_img_convert(img, 2);
	fb_blit(i1, fb, 0, 0);
	getchar();
	fb_img_free(i1);

	i1 = fb_img_convert(img, 1);
	fb_blit(i1, fb, 0, 0);
	getchar();
	fb_img_free(i1);
	}
#endif

#if MINTEST <= 6
	{  printf("test %d...\n", test++);
	if (img) fb_img_free(img);
	/* test: blit an image and then 1 pixel to the left and 1 pixel to the right */
	fb_clear(fb, 0);
	img = fb_img_new(30, 30, 4);
	fb_img_clear(img, fb_img_mkcolor(4, 255, 255, 255, 255));
	fb_blit(img, fb, 59, 10);
	fb_blit(img, fb, 60, 40);
	fb_blit(img, fb, 61, 70);
	getchar();
	}
#endif

#if MINTEST <= 7
	{  printf("test %d...\n", test++);
	if (img) fb_img_free(img);
	/* test: blit image into another image, using all image2image blitting methods */
	img = fb_img_load("testimg.png", 0);
	fb_image* spr = fb_img_load("testspr.png", 4);
	fb_img_blit(spr, img, 0, 0);
	fb_img_blit_alpha(spr, img, 60, 0);
	fb_image *s1 = fb_img_convert(spr, 1);
	fb_img_blit_color_alpha(fb_img_mkcolor(img->ch, 255, 0, 0, 255), img->ch, s1, img, 0, 60);
	fb_img_blit_color_alpha(fb_img_mkcolor(img->ch, 255, 255, 255, 255), img->ch, s1, img, 60, 60);
	fb_blit(img, fb, 0, 0);
	fb_img_free(spr);
	fb_img_free(s1);
	getchar();
	}
#endif

#if MINTEST <= 8
	{  printf("test %d...\n", test++);
	if (img) fb_img_free(img);
	/* test: write some text */
	const char* s1 = "Hallo!!!";
	const char* s2 = "Huhu!!!";
	const char* s3 = "Hallöchen";
	int w, h;
	img = fb_img_new(160, 128, 4);
	fb_img_clear(img, 0);
	fb_font *fnt = fb_txt_loadfont("testfont.ttf");
	fb_txt_strsize(fnt, 10, s1, &w, &h);
	fb_image *i1 = fb_txt_render_alpha(fnt, 10, s1);
	fb_img_blit_color_alpha(fb_img_mkcolor(4, 255, 0, 0, 255), 4, i1, img, 10, 10);

	fb_txt_render(img, fb_img_mkcolor(4, 0, 255, 0, 255), fnt, 12, s2, 10, 30);
	fb_blit(img, fb, 0, 0);

	fb_txt_print(fb, fb_img_mkcolor(4, 0, 0, 255, 255), 4, fnt, 14, s3, 10, 50);
	fb_txt_freefont(fnt);
	fb_img_free(i1);
	getchar();
	}
#endif

	/* --- finalize --- */
	if (img) fb_img_free(img);
	fb_clear(fb, 0);
	fb_close(fb);
	
	exit(0);
}
