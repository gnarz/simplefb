/* simplefb.c
 *
 * provide simple image blitting and stuff for framebuffers
 *
 * Gunnar Zötl <gz@tset.de>, 2014
 * Licensed under the terms of the MIT license. See file LICENSE for details.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <errno.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include "simplefb.h"

#define FB_ERRSTR_SIZE 4096
static int fb_errstr_inuse = 0;
static char* fb_errstr = NULL;

void fb_seterrstr(const char* str, ...)
{
	if (!str) {
		fb_errstr_inuse = 0;
		return;
	}

	if (!fb_errstr)
		fb_errstr = malloc(FB_ERRSTR_SIZE);
	
	va_list ap;
	va_start(ap, str);
	vsnprintf(fb_errstr, FB_ERRSTR_SIZE, str, ap);
	va_end(ap);
	
	fb_errstr_inuse = 1;
}

const char* fb_geterrstr()
{
	if (!fb_errstr_inuse)
		return 0;
	fb_errstr_inuse = 0;
	return fb_errstr;
}

fb_framebuf* fb_open(const char* fb_name)
{
	fb_framebuf* fb = NULL;

    /* open fb_framebuf device in read write */
    int fd = open(fb_name, O_RDWR);
    if (fd < 0) {
        fb_seterrstr("open %s failed: %s\n", fb_name, strerror(errno));
        return 0;
    }
    /* get fixed screen info. */
    struct fb_fix_screeninfo fix;
    if (ioctl(fd, FBIOGET_FSCREENINFO, &fix) < 0) {
        fb_seterrstr("get fixed screen info failed: %s\n", strerror(errno));
        close(fd);
        return 0;
    }
    /* TODO: check for sane type */
    if (fix.visual != FB_VISUAL_TRUECOLOR || fix.type != FB_TYPE_PACKED_PIXELS) {
    	fb_seterrstr("invalid visual\n");
    	close(fd);
    	return 0;
    }
    
    /* get variable screen info. */
    struct fb_var_screeninfo var;
    if (ioctl(fd, FBIOGET_VSCREENINFO, &var) < 0) {
        printf("get variable screen info failed: %s\n", strerror(errno));
        close(fd);
        return 0;
    }
    /* TODO: check for sane type */
    if (var.bits_per_pixel != 16 && var.bits_per_pixel != 32) {
    	fb_seterrstr("invalid pixel size\n");
    	close(fd);
    	return 0;
    }

    /* Now mmap the fb_framebuf. */
    void *mem = mmap(NULL, fix.smem_len, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    if (mem == NULL) {
        fb_seterrstr("mmap failed: %s\n", strerror(errno));
        close(fd);
        return 0;
    }
    
    fb = calloc(1, sizeof(fb_framebuf));
    if (fb == NULL) {
        fb_seterrstr("allocate fb_framebuf failed: %s\n", strerror(errno));
        munmap(mem, fix.smem_len);
        close(fd);
        return 0;
	}
    fb->fd = fd;
    fb->mem = mem;
    fb->fix = fix;
    fb->var = var;
    
    return fb;
}

int fb_close(fb_framebuf *fb)
{
	int ok = 0;
	if (munmap(fb->mem, fb->fix.smem_len) != 0) {
		fb_seterrstr("munmap failed: %s\n", strerror(errno));
		ok = -1;
	} else
		fb->mem = 0;
	if (close(fb->fd) != 0) {
		fb_seterrstr("close fb_framebuf failed: %s\n", strerror(errno));
		ok = -1;
	} else
		fb->fd = -1;
	memset(&fb->fix, 0, sizeof(struct fb_fix_screeninfo));
	memset(&fb->var, 0, sizeof(struct fb_var_screeninfo));
	if (ok == 0) free(fb);
	
	return ok == 0;
}

void fb_fillrect(fb_framebuf *fb, uint32_t col, int x, int y, int w, int h)
{
	// TODO
}

void fb_clear(fb_framebuf *fb, uint32_t col)
{
	uint32_t l = fb->fix.smem_len / 4, i, val = col;
	if (fb->var.bits_per_pixel == 16) {
		val &= 0xFFFF;
		val = (val << 16) | (col & 0xFFFF);
	}
	uint32_t *p = (uint32_t*) fb->mem;
	for (i = 0; i < l; ++i)
		*p++ = val;
}

void fb_setpixel(fb_framebuf *fb, uint32_t x, uint32_t y, uint32_t col)
{
	if (x >= fb->var.xres || y >= fb->var.yres) return;
	int bpp = fb->var.bits_per_pixel == 16 ? 2 : 4;
	if (bpp == 2) {
		uint16_t *p = ((uint16_t*)fb->mem) + y * fb->var.xres + x;
		*p = (uint16_t) col & 0xFFFF;
	} else {
		uint32_t *p = ((uint32_t*)fb->mem) + y * fb->var.xres + x;
		*p = col;
	}
}

int fb_blit(fb_image *img, fb_framebuf *fb, int xofs, int yofs)
{
	int ixofs = 0, iyofs = 0;
	int iw = img->w, ih = img->h;
	
	if ((xofs > (int)fb->var.xres) || (yofs > (int)fb->var.yres)) return 0;
	
	if (xofs < 0) { ixofs = -xofs; xofs = 0; }
	if (yofs < 0) { iyofs = -yofs; yofs = 0; }
	if ((ixofs >= iw) || (iyofs >= ih)) return 0;
	
	iw -= ixofs;
	if (xofs + iw > (int)fb->var.xres) {
		iw = fb->var.xres - xofs;
		if (iw <= 0) return 0;
	}
	ih -= iyofs;
	if (yofs + ih > (int)fb->var.yres) {
		ih = fb->var.yres - yofs;
		if (ih <= 0) return 0;
	}

	uint32_t x, y, col, val, first = 0, last = 0;
	uint32_t xinc = (fb->var.bits_per_pixel == 32) ? 1 : 2, ich = img->ch;
	uint8_t *s;
	uint32_t *t;
	if (xinc == 2) {
		if ((xofs + iw) & 1)
			last = 1;
		if (xofs & 1) {
			iw -= 1;
			first = 1;
		}
	}
	for (y = 0; y < ih; ++y) {
		s = img->mem + (y * img->w + ixofs) * ich;
		t = ((uint32_t*)fb->mem) + ((yofs + y) * fb->var.xres + xofs) / xinc;
		/* handle odd case for xofs when xinc == 2 */
		if (first) {
			uint16_t *th = (uint16_t*)t + 1;
			col = fb_img_getpixel_at(s, img->ch);
			s += ich;
			*th = fb_cvtcolor(fb, col, img->ch);
			++th;
			t = (uint32_t*) th;
		}
		
		/* main image data copying loop */
		for (x = 0; x + xinc <= iw; x += xinc) {
			col = fb_img_getpixel_at(s, img->ch);
			s += ich;
			val = fb_cvtcolor(fb, col, img->ch);
			if (xinc == 2) {
				col = fb_img_getpixel_at(s, img->ch);
				s += ich;
				val = (val & 0xFFFF) | (fb_cvtcolor(fb, col, img->ch) << 16);
			}
			*t++ = val;
		}
		
		/* handle odd case for xofs + iw when xinc==2 */
		if (last) {
			uint16_t *th = (uint16_t*)t;
			col = fb_img_getpixel_at(s, img->ch);
			s += ich;
			*th = fb_cvtcolor(fb, col, img->ch);
			++th;
			t = (uint32_t*) th;
		}
	}
	return 1;
}
